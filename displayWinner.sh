displayWinner(){
#cette fonction prend en paramètre le dossier de jouer 1 et 2
if [ $(isDone $1) -eq "0" && $(isDone $2) eq "1" ]
then   
    echo "Player 1 wins!"
    exit
elif  [ $(isDone $2) -eq "0" && $(isDone $1) eq "1" ]
then 
    echo "Player 2 wins!"
    exit
else 
    echo "The game has not finished!"
    exit
fi
}
