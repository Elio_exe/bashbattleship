#!/bin/bash

source get.sh
source isTouch.sh
source isDone.sh
source displaywinner.sh
source updateShoot.sh
source displayBoard.sh

shoot(){
  echo "-------------------------------------------- Player $1 --------------------------------------------"
  echo "Choissisez ou vous voulez tirer :"
  player=$1
  path=$2
  if [ $player = "j1" ]
  then
    opponent="j2"
  else
    opponent="j1"
  fi
  displayBoard $path/$opponent
  x=$(getx)
  y=$(gety)
  check=$(cat $path/$opponent/$x/$y)
  updateShoot $opponent $x $y $path
  if [ $check = "1" ]
  then
    echo "Vous avez touché un bateau de votre adversaire ! rejouez"

    if [ $(isDone $path/$opponent) -ne 0 ]
    then

      shoot $player $path
    else
      displayWinner j1 J2
    fi
  else
    echo "Vous n'avez pas touché de bateau de votre adversaire"
    clear
  fi
}
