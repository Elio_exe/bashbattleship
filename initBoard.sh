#!/bin/bash

initBoard() {

local numeroJoueur
local i=0
local j=0
local dossier

dossier=$(mktemp -d) #création du dossier temporaire

for numeroJoueur in 1 2
do
	mkdir $dossier/j$numeroJoueur
	for i in $(seq 0 $1)	#$1 à prendre en pipe de la fonction askSize / création des dossiers lignes
	do
		mkdir $dossier/j$numeroJoueur/$i #ligne
		for j in $(seq 0 $1)
		do
			touch $dossier/j$numeroJoueur/$i/$j #colonne
			echo "2" >  $dossier/j$numeroJoueur/$i/$j
		done
	done
done
echo $dossier

}
