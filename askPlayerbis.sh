#!/bin/bash
source get.sh


askPlayerBoat(){
    player=$1
    path=$2
    if [ $player -eq 1 ]
    then
        echo " Player 1"
    else
        echo "Player 2"
    fi
    for nbcase in 5 4 3 3 2
    do
        echo "Vous etes en train de place un bateau de $nbcase case"
        
        echo "Ou voulez vous placer vos bateaux"
        x=$(getx)
        y=$(gety)
	while [ $(cat $path/j$player/$x/$y) -eq 1 ]
	do
		echo "L'emplacement choisi a déjà un bateau, veuillez indiquer d'autres coordonnées"
		x=$(getx)
		y=$(gety)
 	done

        echo "Quelle direction?"
        options2=("Horizontale" "Verticale" )
        select opt2 in "${options2[@]}"
        do
        case $opt2 in
            "Horizontale")
                echo "horizontale choisie"
                direction = hori
                break
                ;;
            "Verticale")
                echo "verticale choisie"
                direction = verti
                break
                ;;
            *) echo "invalid option $REPLY";;
        esac
        done


        if [ $direction == "hori" ] 
        then 
            for i in $(seq 1 $nombre_de_case)
            do
                if [ -e $path/j$player/$x/$y ]
                then 
                    echo "Bateau dépasse du bord. Tant pis pour toi"
                    break
                else 
                echo 1 > $path/j$player/$x/$y
                y=$($y+1)
                fi
            done

        elif [ $direction == "verti" ]
        then 
            for i in $(seq 1 $nombre_de_case)
            do
                if [ -e $path/j$player/$x/$y ]
                then 
                    echo "Bateau dépasse du bord. Tant pis pour toi"
                    break
                else 
                echo 1 > $path/j$player/$x/$y
                x=$($x+1)
                fi
            done
        fi
    done
}

#askPlayerBoat $1 $2


