#!/bin/bash
# Bash Menu Script Example

PS3='Choisis ton niveau: '
options=("Niveau 1" "Niveau 2" "Niveau 3" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Niveau 1")
            echo "Niveau facile"
                # main
            ;;
        "Niveau 2")
            echo "Niveau Intermédiare contre l'ordi"
            break
            ;;
        "Niveau 3")
            echo "Niveau en réseau"
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
