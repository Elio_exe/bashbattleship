# $dossier/j$numJoueur/l$numLigne/c$numColonne
displayBoard() {
#cette fonction demande $dossier/j$numJoueur/ comme paramètre $1
dossierJoueur=$1
for Ligne in $dossierJoueur/*
do
    for Colonne in $Ligne/*
    do
        contenu=$(cat $Colonne)
        if [ $contenu -eq 1 ] || [ $contenu -eq 2 ]
	then
            echo -n 'X'
        elif [ $contenu -eq 3 ]
	then
            echo -n ' '
        elif [ $contenu -eq 4 ]
	then
            echo -n 'B'
        fi
    done
    echo #end of each line
done
}
displayBoardInit() {
#cette fonction demande $dossier/j$numJoueur/ comme paramètre $1
dossierJoueur=$1
for Ligne in $dossierJoueur/*
do
    for Colonne in $Ligne/*
    do
        contenu=$(cat $Colonne)
        if [ $contenu -eq 1 ]
	       then
            echo -n 'B'
        elif [ $contenu -eq 2 ]
  	     then
              echo -n 'X'
        fi
    done
    echo #end of each line
done
}
