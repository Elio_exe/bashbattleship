#!/bin/bash
source get.sh


askPlayerBoat(){
  player=$1
  path=$2
  if [ $player -eq 1 ]
  then
    echo "--------------------------------------------Player 1 --------------------------------------------"
  else
    echo "--------------------------------------------Player 2--------------------------------------------"
  fi
  echo "Où voulez vous placer vos bateaux"
  echo "Vous avez 6 coordonnées à placer"
  for i in $(seq 1 6)
    do
       echo "Placez le point n°$i"
      x=$(getx)
      y=$(gety)
	while [ $(cat $path/j$player/$x/$y) -eq 1 ]
	do
		echo "L'emplacement choisi a déjà un bateau, veuillez indiquer d'autres coordonnées"
		x=$(getx)
		y=$(gety)
 	done
      echo 1 > $path/j$player/$x/$y
    done
}

#askPlayerBoat $1 $2
